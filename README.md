### SpringCRUDProject

## 기본 CRUD 이용한 게시판 
  - 파일 업로드 / 다운로드 (이미지 포함)
  - 페이징
  - 검색
  - 댓글(REST)
  - Spring security
  - 로그인 / 로그아웃 / 자동 로그인
  - 트랜잭션 설정
  - AOP 설정
  
